import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-servers",
  templateUrl: "./servers.component.html",
  styleUrls: ["./servers.component.scss"]
})
export class ServersComponent implements OnInit {
  // allowNewServer = false;
  // allowInnerText = "The button is disabled";
  serverCreationStatus = "No server was created";
  serverName = "Testserver";
  whenServerCreated = false;
  servers = ["Testserver", "Testserver 2"];
       constructor() {
    // setTimeout(() => {
    //   this.allowNewServer = true;
    //   this.allowInnerText = "The Button is Enabled";
    // }, 5000);
  } 

  ngOnInit() {}

  onCreateServer() {
    this.whenServerCreated = true;
    this.servers.push(this.serverName);

    this.serverCreationStatus =
      "Server was created | Name is " + this.serverName;
  }

  onServerUpdate(event: Event) {
    this.serverName = (<HTMLInputElement>event.target).value;
  }
}
